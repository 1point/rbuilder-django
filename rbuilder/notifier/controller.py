from requests import get, exceptions
from .models import Girl, Observer

import vk
import time


def get_telegram_id(username):  # DONE!
    key = '252147620:AAG8XpfXoZJeiS1mNVIruVquMDEX7PB835E'
    try:
        data = get('https://api.telegram.org/bot' + key + '/getupdates')
        for item in data.json()['result']:
            if item['message']['from']['username'] == username:
                return item['message']['from']['id']
            return "please, input valid username and send '/start' to @RbuilderBot"
    except exceptions.ConnectionError:
        return "Oops, something's wrong with internet connection. Please, try again"


def add_girls_list(uid, city):
    start = time.time()

    session = vk.Session()
    api = vk.API(session)

    friends = api.friends.get(user_id=uid,
                              fields=['sex', 'relation', 'city'])

    girlfriends = list()
    ids = set()

    for friend in friends:
        try:
            if friend['sex'] == 1 and friend['city'] == city:
                girlfriends.append(friend)
                ids.add(friend['uid'])
        except KeyError:
            pass  # some actions may be added in the future

    for friend in friends:
        friends_friends = api.friends.get(user_id=friend['user_id'],
                                          fields=['sex', 'relation', 'city'])
        for friends_friend in friends_friends:
            if friends_friend['uid'] in ids:
                continue
            try:
                if friends_friend['sex'] == 1 and friends_friend['city'] == city:
                    girlfriends.append(friends_friend)
                    ids.add(friends_friend['uid'])
            except KeyError:
                pass  # some actions may be added in the future

    friends.clear()
    ids.clear()

    for girlfriend in girlfriends:
        if 'relation' not in girlfriend:
            girlfriend['relation'] = 0

        if Girl.objects.all().filter(uid=girlfriend['uid']):
            girl = Girl.objects.get(uid=girlfriend['uid'])
            girl.observers += '%s,' % uid  # ADD NORMAL CONNECTION!!!!
            girl.save()
        else:
            girl = Girl(first_name=girlfriend['first_name'],
                        last_name=girlfriend['last_name'],
                        uid=girlfriend['uid'],
                        relation_status=girlfriend['relation'],
                        observers=uid)
            girl.save()

    end = time.time()

    return end - start


def get_relation(id):
    session = vk.Session()
    api = vk.API(session)
    data = api.users.get(user_ids=id,
                         fields=['relation'])
    return str(data[0]['relation'])


def notify(girl):
    key = '235289209:AAHNihkwFOUEQiY18WPb_TcG5Z0ThPbqfBQ'
    observers = [observer for observer in girl.observers.all()]
    for observer in observers:
        data = get('https://api.telegram.org/bot' +
                   key +
                   '/sendmessage?' +
                   'chat_id=%d' % observer.telegram_id +
                   '&text=%s %s' % girl.first_name % girl.last_name +
                   ' has been changed relationship status')
    print(data)


def search_updates():
    while True:
        girls = Girl.objects.get()
        for girl in girls:
            current_relation_status = get_relation(girl.uid)
            if girl.relation_status != current_relation_status:
                girl.relation_status = current_relation_status
                girl.save()
                notify(girl)

        girls.clear()

print('s')
def add_observer(request):
    if not Girl.objects.all().filter(uid=request['uid']):
        observer = Observer(phone_number=request['phone_number'],
                            uid=request['uid'],
                            telegram_id=request['telegram_id'])
        observer.save()
    else:
        return 'already exist'


