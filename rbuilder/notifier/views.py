from django.shortcuts import render, HttpResponse
from notifier.models import Girl, Observer
from notifier.controller import add_girls_list


def register(request):
    return render(request, 'notifier/register.html')


def main(request):
    return render(request, 'notifier/main.html')


def start(request):
    if request.method == 'GET':
        return HttpResponse('done')