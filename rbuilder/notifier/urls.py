from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^main/', views.main, name='main'),
    url(r'^registration/', views.register, name='register'),
    url(r'add_user/', views.start, name='start'),
    url(r'^add_girls/', views.add_girls_list, name='add_girls'),
    url(r'^add_observers/', views.add_girls_list, name='add_observers')

]