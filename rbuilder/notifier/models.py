from django.db import models

# Create your models here.


class Observer(models.Model):  # DONE
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    phone_number = models.CharField(max_length=16)
    uid = models.CharField(max_length=16)
    telegram_id = models.CharField(max_length=16, default='')

    def __str__(self):
        return self.uid


class Girl(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    uid = models.CharField(max_length=16)
    observers = models.ManyToManyField(Observer)
    relation_status = models.IntegerField(default=0)  # an integer number from 0 to 7

    def __str__(self):
        return self.uid

